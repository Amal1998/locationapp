import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {

  city: string;
  loc1: string;
  src1: string;
  loc2: string;
  src2: string;
  loc3: string;
  src3: string;


  constructor(private route: ActivatedRoute) { }


  ngOnInit() {

    this.city = this.route.snapshot.paramMap.get('city');


    if(this.city == 'Trivandrum')
    {
      this.loc1= "Kovalam";
      this.loc2= "Varkala";
      this.loc3= "Ponmudi";
      this.src1="https://travelplazablog.files.wordpress.com/2014/10/kovalam.jpg";
      this.src2="https://www.holidify.com/images/compressed/5077.jpg";
      this.src3="https://www.selfroadiez.com/wp-content/uploads/2017/11/Ponmudi-815x459.jpg";

    }else if(this.city == "Thrissur"){
      this.loc1= "Athirappilly Waterfalls";
      this.loc2= "Nehru Park";
      this.loc3= "Bible Tower";
      this.src1="https://media-cdn.tripadvisor.com/media/photo-f/0c/e0/fa/3f/in-athirappilly-waterfalls.jpg";
      this.src2="https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.thegarudahotels.com%2Fwp-content%2Fuploads%2F2015%2F07%2Fnehru-park.jpg&f=1";
      this.src3="https://media-cdn.tripadvisor.com/media/photo-s/03/d9/f9/4f/bible-tower.jpg";
    }
      
  }
}


