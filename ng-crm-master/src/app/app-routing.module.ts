import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountListComponent } from './account-list/account-list.component';
import { AccountCreateComponent } from './account-create/account-create.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactCreateComponent } from './contact-create/contact-create.component';
import { FooterComponent } from './footer/footer.component';


const routes: Routes = [
  
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    component: AccountCreateComponent      
  }, 
  {
    path: 'alist',
    component: AccountListComponent,
    children:[
      {path: '', component: FooterComponent}
    ]
  },
  {
    path: 'login',
    component: ContactListComponent      
  },
  {
    path: 'signup',
    component: ContactCreateComponent
  }      
  // },  
  // {
  //   path: 'leads',
  //   component: LeadListComponent      
  // },
  // {
  //   path: 'create-lead',
  //   component: LeadCreateComponent      
  // },    
  // {
  //   path: 'opportunities',
  //   component: OpportunityListComponent      
  // },
  // {
  //   path: 'create-opportunity',
  //   component: OpportunityCreateComponent      
  // }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }