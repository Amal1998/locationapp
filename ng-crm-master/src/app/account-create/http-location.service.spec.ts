import { TestBed, inject } from '@angular/core/testing';

import { HttpLocationService } from './http-location.service';

describe('HttpLocationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpLocationService]
    });
  });

  it('should be created', inject([HttpLocationService], (service: HttpLocationService) => {
    expect(service).toBeTruthy();
  }));
});
