import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpLocationService } from './http-location.service';




@Component({
  selector: 'app-account-create',
  templateUrl: './account-create.component.html',
  styleUrls: ['./account-create.component.css']
})
export class AccountCreateComponent implements OnInit {

   location: string;

  constructor(private router: Router, private httpLocation: HttpLocationService) { }

  ngOnInit() {

  }

  locate() {
    
    this.httpLocation.getLocation().subscribe(data => {
      console.log(data);
      this.location = data;
      this.router.navigate(['/alist', {city: data}]);
    });
  }
}
