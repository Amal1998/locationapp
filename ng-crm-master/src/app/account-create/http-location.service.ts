import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpLocationService {

  constructor( private http: HttpClient) { }


  getLocation(){

    // Use https://ipapi.co/json/ to get the full information
    return this.http.get('https://ipapi.co/city/',  {responseType: 'text'});

  }

}
